# PRUEBA INGENIERIA RESUELVE #

This README would normally document whatever steps are necessary to get your application up and running.

### Dependencias ###

* Net Core 3.1
* PowerShell para correr el script o igual puede ser desde el visual studio, visual studio code o a patin desde CMD

### Para Ejecutar ###

* Solo ejecuta el script run.ps1 y una vez haya descargado sus dependencias levantara la aplicación en el puerto 5000

> .\run.ps1
```console
$ warn: Microsoft.AspNetCore.Server.Kestrel[0]
$   Unable to bind to https://localhost:5001 on the IPv4 loopback > interface: $ 'Intento de acceso a un socket no permitido por sus > permisos de acceso.'.
$ Hosting environment: Development
$ Content root path: C:\Jona\Resuelve\Resuelve.Salarios.Calculadora
$ Now listening on: https://localhost:5001
$ Now listening on: http://localhost:5000
$ Application started. Press Ctrl+C to shut down.

```

### Para Probar ###

* El post se puede hacer a travez de http://localhost:5000/sueldo

### Algo a considerar? ###

* Di por echo que el nombre del equipo en el JSON para Resuelve FC es
"ResuelveFC", si no fuese así se puede cambiar en el NivelRepository desde la constante 

### Incluye el PLUS? ###

* Claro que lo incluye, calcula el monto de otros equipos.