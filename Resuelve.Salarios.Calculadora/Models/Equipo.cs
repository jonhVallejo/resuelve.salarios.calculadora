namespace Resuelve.Salarios.Calculadora.Models
{
    public class Equipo
    {
        public string Nombre { get; set; }
        public int GolesObtenidos { get; set; }
    }
}