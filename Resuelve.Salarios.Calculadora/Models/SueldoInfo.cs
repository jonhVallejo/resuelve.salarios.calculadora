namespace Resuelve.Salarios.Calculadora.Models
{
    public class SueldoInfo
    {
        public string Nombre { get; set; }
        public string Nivel { get; set; }
        public uint Goles_minimos { get; set; }

        public uint Goles { get; set; }
        public uint Sueldo { get; set; }
        public uint Bono { get; set; }
        public int? Sueldo_completo { get; set; }
        public string Equipo { get; set; }
    }
}