namespace Resuelve.Salarios.Calculadora.Models
{
    public class Nivel
    {
        public Nivel(string equipo, string nombre, int golesRequeridos)
        {
            Equipo = equipo;
            Nombre = nombre;
            GolesRequeridos = golesRequeridos;
        }
        public string Equipo { get; set; }
        public string Nombre { get; set; }

        public int GolesRequeridos { get; set; }
    }
}
