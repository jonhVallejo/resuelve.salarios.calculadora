using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Resuelve.Salarios.Calculadora.Models;
using Resuelve.Salarios.Calculadora.Application;
using System.Collections.Generic;

namespace Resuelve.Salarios.Calculadora.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SueldoController : ControllerBase
    {
        private readonly ICalculadoraService _calculadora;
        private readonly IEquipoClasificadorService _clasificador;
        public SueldoController(ICalculadoraService calculadora, IEquipoClasificadorService clasificador)
        {
            _clasificador = clasificador;
            _calculadora = calculadora;
        }

        [HttpGet]
        public async Task<ActionResult<List<SueldoInfo>>> GetAll()
        {
            return await Task.FromResult(new List<SueldoInfo>());
        }

        /// <summary>
        /// Recibe en el request la lista de sueldos que se desea calcular,
        /// posteriormente primero clasifica los equipos enviados y despues hace el calculo de salarios.
        /// Finalmente retorna los salarios calculados.
        /// </summary>
        /// <param name="sueldos"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<SueldoInfo>> PostSueldos(IEnumerable<SueldoInfo> sueldos)
        {

            var equipos = _clasificador.Clasifica(sueldos);

            var sueldosCalculados = _calculadora.Calcula(sueldos, equipos);
            
            return await Task.FromResult(CreatedAtAction("GetAll", sueldosCalculados));
        }
    }
}