using System.Collections.Generic;
using Resuelve.Salarios.Calculadora.Models;

namespace Resuelve.Salarios.Calculadora.Infrastructure
{
    public interface INivelRepository
    {
        void Insert(Nivel nivel);
        Nivel Find(string name, string equipo);

        IEnumerable<Nivel> FindAllByEquipo(string equipo);
    }
}