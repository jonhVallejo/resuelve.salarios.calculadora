using System.Collections.Generic;
using System.Linq;
using Resuelve.Salarios.Calculadora.Models;

namespace Resuelve.Salarios.Calculadora.Infrastructure
{
    /// <summary>
    /// Una representación basica de un repositorio, en el que por defualt se maneja
    /// el equipo ResuelveFC y se da por echo que el nombre del equipo para Resuelve Fc sea
    /// "ResuelveFC", en caso contrario, seria muy facil cambiarlo desde la constante EQUIPO_DEFAULT
    /// </summary>
    public class NivelRepository : INivelRepository
    {
        private List<Nivel> _dataContext;
        private const string EQUIPO_DEFAULT = "ResuelveFC";
        public NivelRepository() 
        {
            _dataContext = new List<Nivel>();
            _dataContext.Add(new Nivel(EQUIPO_DEFAULT, "A", 5));
            _dataContext.Add(new Nivel(EQUIPO_DEFAULT, "B", 10));
            _dataContext.Add(new Nivel(EQUIPO_DEFAULT, "C", 15));
            _dataContext.Add(new Nivel(EQUIPO_DEFAULT, "Cuauh", 20));
        }
        public void Insert(Nivel nivel) => _dataContext.Add(nivel);
        public Nivel Find(string name, string equipo) => _dataContext.Where(_ => _.Nombre == name && _.Nombre == equipo).FirstOrDefault();
        public IEnumerable<Nivel> FindAllByEquipo(string equipo) => _dataContext.Where(_ => _.Equipo == equipo);
    }
}