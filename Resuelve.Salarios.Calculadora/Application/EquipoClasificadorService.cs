using System.Collections.Generic;
using Resuelve.Salarios.Calculadora.Models;
using System.Linq;
using Resuelve.Salarios.Calculadora.Infrastructure;
using System;

namespace Resuelve.Salarios.Calculadora.Application
{
    public class EquipoClasificadorService : IEquipoClasificadorService
    {
        private readonly INivelRepository _nivelRepository;
        public EquipoClasificadorService(INivelRepository nivelRepository) => _nivelRepository = nivelRepository;

        /// <summary>
        /// Clasifica los equipos que son enviados en la lista de salarios,
        /// al mismo tiempo inserta en el repositorio todos los niveles que 
        /// son enviados para poder obtener el numero de goles por equipo.
        ///
        /// Este es un metodo que representa lo que seria un Application Service en DDD (Domain Driven Design)
        /// </summary>
        /// <param name="sueldos"></param>
        /// <param name="equipos"></param>
        /// <returns></returns>
        public IEnumerable<Equipo> Clasifica(IEnumerable<SueldoInfo> sueldos)
        {
            var equipos = sueldos.GroupBy(_ => _.Equipo);

            var clasificados = new List<Equipo>();

            foreach(var equipo in equipos)
            {
                foreach(var item in equipo)
                {
                    if(_nivelRepository.Find(item.Nivel, equipo.Key) == null)
                        _nivelRepository.Insert(new Nivel(equipo.Key, item.Nivel, Convert.ToInt32(item.Goles_minimos)));
                }

                clasificados.Add(new Equipo
                {
                    Nombre = equipo.Key,
                    GolesObtenidos = Convert.ToInt32(equipo.Sum(_ => _.Goles))
                });
            }

            return clasificados;
        }
    }
}