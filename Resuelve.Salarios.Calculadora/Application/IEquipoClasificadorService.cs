using System.Collections.Generic;
using Resuelve.Salarios.Calculadora.Models;


namespace Resuelve.Salarios.Calculadora.Application
{
    public interface IEquipoClasificadorService
    {
        
        IEnumerable<Equipo> Clasifica(IEnumerable<SueldoInfo> sueldos);
    }
}