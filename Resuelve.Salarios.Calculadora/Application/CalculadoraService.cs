using System;
using System.Collections.Generic;
using System.Linq;
using Resuelve.Salarios.Calculadora.Models;
using Resuelve.Salarios.Calculadora.Infrastructure;

namespace Resuelve.Salarios.Calculadora.Application
{
    public class CalculadoraService : ICalculadoraService
    {
        private readonly INivelRepository _nivelRepository;
        public CalculadoraService(INivelRepository nivelRepository) => _nivelRepository = nivelRepository;

        /// <summary>
        /// Calcula el sueldo de cada equipo que se recibe en la lista de sueldos, con ayuda de los equipos ya clasificados
        /// mismos que son enviados en la lista equipos.
        ///
        /// Este es un metodo que representa lo que seria un Application Service en DDD (Domain Driven Design)
        /// </summary>
        /// <param name="sueldos"></param>
        /// <param name="equipos"></param>
        /// <returns></returns>
        public IEnumerable<SueldoInfo> Calcula(IEnumerable<SueldoInfo> sueldos, IEnumerable<Equipo> equipos)
        {
            var sueldosCalculados = new List<SueldoInfo>();
            
            foreach(var equipo in equipos)
            {
                var sueldosEquipoActual = sueldos.Where(_ => _.Equipo == equipo.Nombre);
                
                var porcentajeEquipo = PorcentajeGeneral(sueldosEquipoActual, equipo.Nombre);

                sueldosCalculados.AddRange(sueldosEquipoActual.Select(_ =>
                {
                    var golesRequeridos = _nivelRepository.Find(_.Nivel, _.Equipo)?.GolesRequeridos ?? 0;

                    var porcentajePersonal = PorcentajePersonal(golesRequeridos, Convert.ToInt32(_.Goles));

                    _.Goles_minimos = (uint)golesRequeridos;
                    _.Sueldo_completo = SueldoCompleto(Convert.ToInt32(_.Sueldo), Convert.ToInt32(_.Bono), porcentajeEquipo, porcentajePersonal);
                    
                    return _;
                }));
            }
            return sueldosCalculados;
        }

        private int SueldoCompleto(int sueldo, int bono, decimal porcentajeEquipo, decimal porcentajePersonal)
        {
            var porcentaje = (porcentajeEquipo + porcentajePersonal) / 2;
            
            return sueldo + (int) (bono * porcentaje);
        }

        private decimal PorcentajePersonal(int golesRequeridos, int golesConseguidos)
        {
            return golesConseguidos > golesRequeridos ? 1.0M : (decimal)golesConseguidos / golesRequeridos;
        }

        private decimal PorcentajeGeneral(IEnumerable<SueldoInfo> sueldos, string equipo)
        {
            var golesDelEquipo = sueldos.Sum(_ => _.Goles);

            var golesRequeridosEquipo = _nivelRepository.FindAllByEquipo(equipo).Sum(_ => _.GolesRequeridos);

            return golesDelEquipo > golesRequeridosEquipo ? 1.0M : (decimal)golesDelEquipo / golesRequeridosEquipo;
        }
    }
}