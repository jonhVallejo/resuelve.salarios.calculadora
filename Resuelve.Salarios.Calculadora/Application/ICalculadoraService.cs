using System.Collections.Generic;
using Resuelve.Salarios.Calculadora.Models;


namespace Resuelve.Salarios.Calculadora.Application
{
    public interface ICalculadoraService
    {
        IEnumerable<SueldoInfo> Calcula(IEnumerable<SueldoInfo> sueldos, IEnumerable<Equipo> equipos);
    }
}