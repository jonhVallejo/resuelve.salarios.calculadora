using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Resuelve.Salarios.Calculadora.Application;
using Resuelve.Salarios.Calculadora.Infrastructure;

namespace Resuelve.Salarios.Calculadora
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IContainer ApplicationContainer { get; private set; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            var builder = new ContainerBuilder();
            builder.Populate(services);

            builder.RegisterType<NivelRepository>()
            .As<INivelRepository>();
            builder.RegisterType<CalculadoraService>()
            .As<ICalculadoraService>();
            builder.RegisterType<EquipoClasificadorService>()
            .As<IEquipoClasificadorService>();

            this.ApplicationContainer = builder.Build();

            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
